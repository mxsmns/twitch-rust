use chrono::{DateTime, Local, Utc};
use regex::Regex;
use std::cmp::{Eq, PartialEq};
use std::fmt;
use std::fmt::Debug;

#[derive(Eq, Debug)]
pub struct TwitchMessage {
    user: String,
    message: String,
    timestamp: DateTime<Utc>,
}

impl TwitchMessage {
    pub fn new(user: String, message: String, timestamp: DateTime<Utc>) -> TwitchMessage {
        TwitchMessage {
            user,
            message,
            timestamp,
        }
    }

    pub fn from_string(message: String) -> Option<TwitchMessage> {
        let message_regex =
            Regex::new(r"^:(?P<nick>.*)!.*@.*\sPRIVMSG(?P<channel>.*)\s:(?P<message>.*)").unwrap();
        let caps = match message_regex.captures(&message) {
            Some(captures) => captures,
            None => return None,
        };

        Some(TwitchMessage::new(
            caps["nick"].to_string(),
            caps["message"].to_string(),
            Utc::now(),
        ))
    }
}

impl PartialEq for TwitchMessage {
    fn eq(&self, other: &Self) -> bool {
        self.user == other.user && self.message == other.message
    }
}

impl fmt::Display for TwitchMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} {}: {}",
            self.timestamp
                .with_timezone(&Local)
                .format("%Y-%m-%d %I:%M:%S"),
            self.user,
            self.message
        )
    }
}

pub fn make_authentication(nick: &str, pass: &str, channel: &str) -> [String; 3] {
    [
        format!("PASS {}\n", pass),
        format!("NICK {}\n", nick),
        format!("JOIN #{}\n", channel),
    ]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_make_authentication() {
        let result = make_authentication("test_nick", "test_oauth", "test_channel");
        let expected = [
            "PASS test_oauth\n",
            "NICK test_nick\n",
            "JOIN #test_channel\n",
        ];
        assert_eq!(result, expected)
    }

    #[test]
    fn test_twitchmessage_from_string_chat() {
        let chat_message = String::from(
            ":test_user!test_user@test_user.tmi.twitch.tv PRIVMSG #test_channel :test_message",
        );

        let expected = TwitchMessage {
            user: String::from("test_user"),
            message: String::from("test_message"),
            timestamp: Utc::now(),
        };

        match TwitchMessage::from_string(chat_message) {
            Some(result) => assert_eq!(result, expected),
            _ => panic!("Message parsed incorrectly"),
        }
    }

    #[test]
    fn test_twitchmessage_from_string_status() {
        let chat_message =
            String::from(":test_user!test_user@test_user.tmi.twitch.tv JOIN #test_channel");

        match TwitchMessage::from_string(chat_message) {
            None => (),
            _ => panic!("Message parsed incorrectly"),
        }
    }
}
