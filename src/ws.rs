use std::net::TcpStream;
use std::str::from_utf8;
use websocket::client::sync::Client;
use websocket::message::Type;
use websocket::{ClientBuilder, Message};

use crate::common::{make_authentication, TwitchMessage};

pub struct WebsocketClient {
    client: Client<TcpStream>,
    nick: String,
    pass: String,
    channel: String,
}

impl WebsocketClient {
    pub fn new(nick: &str, pass: &str, channel: &str) -> WebsocketClient {
        let client = ClientBuilder::new("ws://irc-ws.chat.twitch.tv:80")
            .unwrap()
            .connect_insecure()
            .unwrap();
        let mut ws_client = WebsocketClient {
            client,
            nick: String::from(nick),
            pass: String::from(pass),
            channel: String::from(channel),
        };

        ws_client.authenticate();
        ws_client
    }

    pub fn message_loop(&mut self) {
        for message in self.client.incoming_messages() {
            let raw_message = Message::from(message.unwrap());
            match raw_message.opcode {
                Type::Text => {
                    let message = String::from(from_utf8(&raw_message.payload).unwrap());
                    if let Some(message) = TwitchMessage::from_string(message) {
                        println!("{}", message)
                    }
                }
                Type::Close => return,
                _ => (),
            }
        }
    }

    fn authenticate(&mut self) {
        for msg in make_authentication(&self.nick, &self.pass, &self.channel).iter() {
            self.client.send_message(&Message::text(msg)).unwrap();
        }
    }
}
