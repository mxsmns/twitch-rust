extern crate dotenv;

use dotenv::dotenv;

mod common;
mod ws;

use structopt::StructOpt;

use ws::WebsocketClient;

#[derive(StructOpt)]
struct Cli {
    #[structopt(env = "CHANNEL")]
    channel: String,
    #[structopt(env = "NICK")]
    nick: String,
    #[structopt(env = "PASS")]
    oauth: String,
}

fn main() {
    dotenv().ok();
    let args = Cli::from_args();
    let mut client = WebsocketClient::new(&args.nick, &args.oauth, &args.channel);
    client.message_loop();
}
